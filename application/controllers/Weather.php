<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weather extends CI_Controller {

	public function index()
	{
		$this->load->view('template/v_header');
		$this->load->view('pages/v_index');
		$this->load->view('template/v_footer');
	}

	public function ciudades(){
		// $ciudad = $this->input->post('ciudad');
		
		$ciudad = $this->input->post('q');

		$file = base_url().'public/json/city.list.json';
		$jsonitem = file_get_contents($file);
		$arr = json_decode($jsonitem,true);
		$var = $this->filter_array($arr,$ciudad);
		header('content-type: json');
		echo json_encode($var);
		exit();
	}

	private function filter_array($array,$term){
		$matches = array();
		$term = '/'.$term.'/i';
		$i = 0;
		foreach ($array as $key => $a) {
			$valor = preg_match($term, $a['name']);
			if($valor){
				$matches[$i]['Value'] = $a['id'];
				$matches[$i]['DisplayText'] = $a['name'];
				$matches[$i]['DisplayPais'] = $a['country'];
				$matches[$i]['DisplayEstado'] = $a['state'];
				$matches[$i]['DisplayCoord'] = $a['coord'];
				$i++;
			}
		}
		return $matches;
	}

	public function hoy(){
		$ciudad = $this->input->post('ciudad');

		$dias = $this->input->post('diasAtras');

		
		$api = "api.openweathermap.org/data/2.5/weather?appid=".APIKEY."&q=".$ciudad;
		

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $api,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;

	}

	public function pronostico(){
		$ciudad = $this->input->post('ciudad');
		$apiKey = "2cf524eea7037ee3145a1478a8f578af";
		$api = "api.openweathermap.org/data/2.5/forecast?&q=".$ciudad."&appid=".APIKEY;

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $api,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;
	}
}

/* End of file Weather.php */
/* Location: ./application/controllers/Weather.php */