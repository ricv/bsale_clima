<div id="mapa">
	
</div>

<div class="p-2 form">
	<div class="fondoSearch px-2 pt-4 pb-2">
		<div class="input-group mb-3 formHeader">
			<select class="form-control selectpicker with-ajax" id="selectCiudad" data-live-search="true">
			</select>
			<div class="input-group-append">
				<button class="btn btn-outline-light text-secondary btnHeader" id="btnSearch" type="button"><i class="fas fa-search"></i></button>
			</div>
		</div>
		<div id="filter" class="row collapse pb-2">
			<div class="col-12">
				<div id="accordion">
					
					
				</div>
			</div>
		</div>
		<button class="btn btn-sm btn-secondary float-right btnShow" ><span class="fas fa-arrow-down"></span> Ver pronosticos 5 dias proximos</button>
		<button class="btn btn-sm btn-secondary float-right btnHide" style="display: none" ><span class="fas fa-arrow-up"></span> Cerrar</button>
	</div>
</div>
<div class="info">
	

	
	
</div>


<script id="templateFicha" type="x-tmpl-mustache">
	<div class="divInfo pt-2">
		
		{{#weather}}
			<div class="clima">
				<div class='img'>
					<img src='http://openweathermap.org/img/wn/{{icon}}.png' class='imageTempo'>
				</div>
				<div class='description'>{{ description }}</div>
			</div>
		{{/weather}}
		<h3>					
			<span class="iNombre">{{ iNombre }}</span>
		</h3>
		<div class="d-flex flex-column col-4 row">
			<div class="d-flex justify-content-between">
				<small>
					<span class="fas fa-arrow-up"></span> {{ temp_max }}
				</small>
				<small>
					<span class="fas fa-arrow-down"></span> {{ temp_min }}</span>
				</small>
			</div>
			<div class="flex justify-content-center">
				<h1 class="flex justify-content-center align-items-center text-center">
					{{ temp }} 
				</h1>	
			</div>

		</div>

		<div class="row pb-2">
			<div class="col-6">
				<b>Nivel del suelo: </b><span class="grnd_level">{{grnd_level}}</span>
			</div>
			<div class="col-6">
				<b>Humedad: </b><span class="humidity">{{humidity}}</span>
			</div>
			<div class="col-6">
				<b>Presión: </b><span class="pressure">{{pressure}}</span>
			</div>
			<div class="col-6">
				<b>el nivel del mar: </b><span class="sea_level">{{sea_level}}</span>
			</div>
		</div>
		<div>
			<h4 class="border border-dark border-left-0 border-right-0 border-top-0">Viento</h4>
			<div class="row">
				<div class="col-6">
					<b>Dirección: </b><span class="deg">{{deg}}</span>
				</div>
				<div class="col-6">
					<b>velocidad: </b><span class="speed">{{speed}}</span>
				</div>
			</div>
		</div>
	</div>
</script>


<script id="templateFichaHistorica" type="x-tmpl-mustache">

	<div class="card">
		<div class="card-header">
			<a class="card-link" data-toggle="collapse" href="#collapseOne">
				{{fecha}}
			</a>
		</div>
		<div id="collapseOne" class="collapse" data-parent="#accordion">
			<div class="card-body">
				<div class="divInfo pt-2">
					{{#weather}}
						<div class="clima">
							<div class='img'>
								<img src='http://openweathermap.org/img/wn/{{icon}}.png' class='imageTempo'>
							</div>
							<div class='description'>{{ description }}</div>
						</div>
					{{/weather}}
					<div class="d-flex flex-column col-4 row">
						<div class="d-flex justify-content-between">
							<small>
								<span class="fas fa-arrow-up"></span> {{ temp_max }}
							</small>
							<small>
								<span class="fas fa-arrow-down"></span> {{ temp_min }}</span>
							</small>
						</div>
						<div class="flex justify-content-center">
							<h1 class="flex justify-content-center align-items-center text-center">
								{{ temp }}
							</h1>	
						</div>

					</div>

					<div class="row pb-2">
						<div class="col-6">
							<b>Nivel del suelo: </b><span class="grnd_level">{{grnd_level}}</span>
						</div>
						<div class="col-6">
							<b>Humedad: </b><span class="humidity">{{humidity}}</span>
						</div>
						<div class="col-6">
							<b>Presión: </b><span class="pressure">{{pressure}}</span>
						</div>
						<div class="col-6">
							<b>el nivel del mar: </b><span class="sea_level">{{sea_level}}</span>
						</div>
					</div>
					<div>
						<h4 class="border border-dark border-left-0 border-right-0 border-top-0">Viento</h4>
						<div class="row">
							<div class="col-6">
								<b>Dirección: </b><span class="deg">{{deg}}</span>
							</div>
							<div class="col-6">
								<b>velocidad: </b><span class="speed">{{speed}}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</script>