$.fn.selectpicker.Constructor.BootstrapVersion = '4';

var select = $("#selectCiudad").selectpicker()
select.filter('.with-ajax').ajaxSelectPicker({
	ajax : {
		url     : base_url+'weather/ciudades',
		type    : 'POST',
		dataType: 'json',
		data    : {
			q: '{{{q}}}',
		}
	},
	log : 0,
	width: 'auto',
	minLength: 3,
	mobile: true,
	preserveSelected: false,
	preprocessData: function (data) {
		var i, l = data.length, array = [];
		if (l) {
			for (i = 0; i < l; i++) {
				array.push($.extend(true, data[i], {
					text : data[i].DisplayText,
					value: data[i].DisplayText,
					data : {
						subtext: data[i].DisplayPais
					}
				}));
			}
		}
		return array;
	}
}).on('changed.bs.select', function(e, clickedIndex, isSelected, previousValue, data){
	var ciudad = e.target[1].textContent;
	tempo(ciudad);
	pronostico(ciudad);
});



$.getJSON('http://api.wipmania.com/jsonp?callback=?', function (data) {
	console.log(data);
	localStorage.setItem("markerLat", data.latitude);
	localStorage.setItem("markerLong", data.longitude);
	localStorage.setItem("dataZoom", data.zoom);
	console.log(data.address);
	console.log(data.address.country);
});

$("#btnSearch").click(function(event) {
	var ciudad = $("#selectCiudad").val();
	tempo(ciudad);
	pronostico(ciudad);
});
var marker;
var myLatLng;
var map;

function pintaMapa(){
	var dataLat = parseFloat(localStorage.getItem('markerLat'));
	var dataLong = parseFloat(localStorage.getItem('markerLong'));
	var dataZoom = parseFloat(localStorage.getItem('dataZoom'));
	myLatLng = {lat: dataLat, lng: dataLong};
	map = new google.maps.Map(document.getElementById('mapa'), {
		center: myLatLng,
		zoom: dataZoom
	});
}

$(document).ready(function($) {
	pintaMapa()
});

var datosTiempo;


function pronostico(ciudad){
	if (!ciudad) {
		alertify.error('Busque una ciudad.').dismissOthers(); 
		return false;
	}
	$.ajax({
		url: base_url+'Weather/pronostico',
		type: 'POST',
		dataType: 'json',
		data: {
			ciudad: ciudad
		},
	})
	.done(function(response) {
		datosHistorico(response);
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}
function tempo(ciudad){
	if (!ciudad) {
		alertify.error('Busque una ciudad.').dismissOthers(); 
		return false;
	}
	var diasAtras = $("#cuando").val();

	$.ajax({
		url: base_url+'Weather/hoy',
		type: 'POST',
		dataType: 'json',
		data: {
			ciudad: ciudad
		},
	})
	.done(function(response) {
		datosTiempo(response);
		myLatLng = {lat: response.coord.lat, lng: response.coord.lon};
		map = new google.maps.Map(document.getElementById('mapa'), {
			center: myLatLng,
			zoom: 12
		});

		marker = new google.maps.Marker({
			position: myLatLng,
			map: map,

		});
		
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}



function datosTiempo(datos){
	console.log(datos);
	var template = document.getElementById('templateFicha').innerHTML;
	var rendered = Mustache.render(template, { 
		iNombre: datos.name,
		feels_like: datos.main.feels_like,
		grnd_level: datos.main.grnd_level,
		humidity: datos.main.humidity+'%',
		pressure: datos.main.pressure,
		sea_level: datos.main.sea_level,
		temp: temperatura(datos.main.temp),
		temp_max: temperatura(datos.main.temp_max),
		temp_min: temperatura(datos.main.temp_min),
		deg: (datos.wind.deg+"º"),
		speed: datos.wind.speed,
		weather: datos.weather,
	});
	$(".info").html(rendered)
	$(".info").show()
}

function temperatura(valor){
	return (Math.round(valor - 273.15)+'ºC');
}

function datosHistorico(datos){
	console.log(datos);
	

	var template = document.getElementById('templateFichaHistorica').innerHTML;
	$("#accordion").html('');
	$.each(datos.list, function(index, val) {
		console.log(val);
		
		var rendered = Mustache.render(template, {
			temp_max	: temperatura(val.main.temp_max),
			temp_min	: temperatura(val.main.temp_min),
			temp 		: temperatura(val.main.temp),
			fecha		: val.dt_txt,
			weather 	: val.weather,
			main 		: val.main,
			grnd_level	: val.main.grnd_level,
			humidity	: val.main.humidity,
			pressure	: val.main.pressure,
			sea_level	: val.main.sea_level,
			deg			: val.wind.deg + 'º',
			speed		: val.wind.speed
		})
		$("#accordion").append(rendered)
		

	});

}





$(".btnShow").click(function(){
	$("#filter").collapse('show');
	$(this).hide();
	$(".btnHide").show();
})

$(".btnHide").click(function(){
	$("#filter").collapse('hide');
	$(this).hide();
	$(".btnShow").show();
})